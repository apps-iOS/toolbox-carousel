//
//  LoginPresenter.swift
//  ToolboxCarousel
//
//  Created by Daniel on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LoginPresenterDelegate: class {
    func loginDidFinish(login: Login?, message: String?)
}

class LoginPresenter {
    weak var delegate: LoginPresenterDelegate?
    let service: ToolboxService = ToolboxService()
    
    init(delegate: LoginPresenterDelegate) {
        self.delegate = delegate
    }
    
    func login(withCode code: String) {
        self.service.loginService(loginCode: code.trimmingCharacters(in: NSCharacterSet.whitespaces), completion: { json in
            guard (json != nil) else {
                self.delegate?.loginDidFinish(login: nil, message: "No se pudo conectar")
                return
            }
            guard ((json?["token"].stringValue)! != "") else {
                self.delegate?.loginDidFinish(login: nil, message: "El código ingresado no es correcto")
                return
            }
            let login = Login(sub: (json?["sub"].stringValue)!, token: (json?["token"].stringValue)!, type: (json?["type"].stringValue)!)
            self.delegate?.loginDidFinish(login: login, message: nil)
        })
    }
}
