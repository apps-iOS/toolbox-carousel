//
//  DataPresenter.swift
//  ToolboxCarousel
//
//  Created by Daniel on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation

protocol DataPresenterDelegate: class {
    func getDataDidFinish(data: [Data]?)
}

class DataPresenter {
    weak var delegate: DataPresenterDelegate?
    let service: ToolboxService = ToolboxService()
    
    init(delegate: DataPresenterDelegate) {
        self.delegate = delegate
    }
    
    func getData(withAuth auth: Login) {
        self.service.getDataService(login: auth, completion: { json in
            guard (json != nil) else {
                self.delegate?.getDataDidFinish(data: nil)
                return
            }
            var data: [Data] = []
            for item in (json?.array)! {
                var items: [Item] = []
                for it in item["items"].arrayValue {
                    items.append(Item(title: it["title"].stringValue, imageUrl: it["imageUrl"].stringValue, videoUrl: it["videoUrl"].stringValue, description: it["description"].stringValue))
                }
                data.append(Data(title: item["title"].stringValue, type: item["type"].stringValue == "thumb" ? .thumb : .poster, items: items))
            }
            self.delegate?.getDataDidFinish(data: data)
        })
    }
}
