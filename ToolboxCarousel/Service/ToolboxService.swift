//
//  ToolboxService.swift
//  ToolboxCarousel
//
//  Created by Daniel on 09/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ToolboxService {
    static let URL_BASE = "https://echo-serv.tbxnet.com/"
    static let URL_LOGIN = URL_BASE + "v1/mobile/auth"
    static let URL_DATA = URL_BASE + "v1/mobile/data"
    
    func loginService(loginCode: String, completion: @escaping(JSON?) -> Void) {
        Alamofire.request(ToolboxService.URL_LOGIN,
                          method: .post,
                          parameters: ["sub": loginCode])
            .responseJSON { response in
            switch response.result {
                case .success(let data):
                    completion(JSON(data))
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(nil)
            }
        }
    }
    
    func getDataService(login: Login, completion: @escaping(JSON?) -> Void) {
        Alamofire.request(ToolboxService.URL_DATA,
                          method: .get,
                          headers: ["Authorization": "\(login.type) \(login.token)"])
            .responseJSON { response in
            switch response.result {
                case .success(let data):
                    completion(JSON(data))
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(nil)
            }
        }
    }
}
