//
//  LoginViewController.swift
//  ToolboxCarousel
//
//  Created by Daniel on 09/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var presenter: LoginPresenter!
    var login: Login!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginButton.round()
        self.codeTextField.redRoundBorder()
        self.presenter = LoginPresenter(delegate: self)
    }
    
    @IBAction func login(_ sender: Any) {
        self.codeTextField.resignFirstResponder()
        self.loginButton.isEnabled = false
        self.presenter.login(withCode: self.codeTextField.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openData" {
            let dataVC = segue.destination as? DataViewController
            dataVC?.auth = self.login
        }
    }
    
}

extension UIButton {
    func round() {
        layer.cornerRadius = 10 //bounds.height / 2
        clipsToBounds = true
    }
}

extension UITextField {
    func redRoundBorder() {
        let myColor : UIColor = UIColor(red:0.87, green:0.14, blue:0.20, alpha:1.0) // HEX: #dd2432 | RGB: 221,36,50
        layer.cornerRadius = 10
        layer.borderWidth = 1.5
        layer.borderColor = myColor.cgColor
        layer.masksToBounds = true
    }
}

extension LoginViewController: LoginPresenterDelegate {
    func loginDidFinish(login: Login?, message: String?) {
        self.loginButton.isEnabled = true
        guard (login != nil) else {
            let alert = UIAlertController(title: "Error", message: message!, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.codeTextField.text = ""
            return
        }
        self.login = login!
        performSegue(withIdentifier: "openData", sender: self)
    }
}
