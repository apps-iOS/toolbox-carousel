//
//  ItemThumbCollectionViewCell.swift
//  ToolboxCarousel
//
//  Created by Daniel on 12/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit
import AlamofireImage

class ItemThumbCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.image.layer.cornerRadius = 10
        self.image.clipsToBounds = true
    }
    
    func loadImage(image: String) {
        let img = image.replacingOccurrences(of: "http:", with: "https:")
        if image == "" {
            self.image.image = UIImage(named: "no-image-thumb")
        } else {
            if let imageURL = URL(string: img), let placeholder = UIImage(named: "no-image-thumb") {
                self.image.af_setImage(withURL: imageURL, placeholderImage: placeholder)
            }
        }
    }
}
