//
//  ItemPosterCollectionViewCell.swift
//  ToolboxCarousel
//
//  Created by Daniel on 13/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit

class ItemPosterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadImage(image: String) {
        let img = image.replacingOccurrences(of: "http:", with: "https:")
        if image == "" {
            self.image.image = UIImage(named: "no-image-poster")
        } else {
            if let imageURL = URL(string: img), let placeholder = UIImage(named: "no-image-poster") {
                self.image.af_setImage(withURL: imageURL, placeholderImage: placeholder)
            }
        }
        self.image.layer.cornerRadius = 50
        self.image.clipsToBounds = true
    }
}
