//
//  DataTableViewCell.swift
//  ToolboxCarousel
//
//  Created by Daniel on 11/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit
import AlamofireImage

class DataTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dataCollectionView: UICollectionView!
    var items: [Item] = []
    var type : Type!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 300, height: 300)
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        self.dataCollectionView.collectionViewLayout = flowLayout
        
        self.dataCollectionView.delegate = self
        self.dataCollectionView.dataSource = self
        self.dataCollectionView.register(UINib.init(nibName: "ItemThumbCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "thumbItem")
        self.dataCollectionView.register(UINib.init(nibName: "ItemPosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "posterItem")
//        self.dataCollectionView.register(ItemThumbCollectionViewCell.self, forCellWithReuseIdentifier: "thumbItem")
//        self.dataCollectionView.register(ItemPosterCollectionViewCell.self, forCellWithReuseIdentifier: "posterItem")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepareCell(withItem item: Item, forRowAt indexPath: IndexPath) -> UICollectionViewCell {
        switch self.type {
            case .thumb:
                let cell = self.dataCollectionView.dequeueReusableCell(withReuseIdentifier: "thumbItem", for: indexPath) as! ItemThumbCollectionViewCell
                cell.titleLabel?.text = item.title != "" ? item.title : ""
                cell.loadImage(image: item.imageUrl)
                return cell
            case .poster:
                let cell = self.dataCollectionView.dequeueReusableCell(withReuseIdentifier: "posterItem", for: indexPath) as! ItemPosterCollectionViewCell
                cell.titleLabel?.text = item.title != "" ? item.title : ""
                cell.loadImage(image: item.imageUrl)
                return cell
            case .none:
                let cell = self.dataCollectionView.dequeueReusableCell(withReuseIdentifier: "posterItem", for: indexPath)
                cell.largeContentTitle = item.title != "" ? item.title : ""
                return cell
        }
    }
}

extension DataTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item: Item = self.items[indexPath.row]
        return self.prepareCell(withItem: item, forRowAt: indexPath)
    }
}
