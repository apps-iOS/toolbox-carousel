//
//  DataViewController.swift
//  ToolboxCarousel
//
//  Created by Daniel on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var presenter: DataPresenter!
    var auth: Login!
    var data: [Data] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "DataTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.presenter = DataPresenter(delegate: self)
        self.presenter.getData(withAuth: auth)
    }
}

extension DataViewController: DataPresenterDelegate {
    func getDataDidFinish(data: [Data]?) {
        print(data!)
        guard (data != nil) else {
            let alert = UIAlertController(title: "Error", message: "No se ha podido conectar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        self.data = data!
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 320
        self.tableView.reloadData()
    }
}

extension DataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DataTableViewCell
        let data = self.data[indexPath.row]
        cell.titleLabel.text = data.title
        cell.items = data.items
        cell.type = data.type
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
