//
//  Type.swift
//  ToolboxCarousel
//
//  Created by Daniel Gomez on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation

enum Type {
    case thumb
    case poster
}
