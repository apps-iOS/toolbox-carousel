//
//  Data.swift
//  ToolboxCarousel
//
//  Created by Daniel on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation

struct Data {
    var title: String
    var type: Type
    var items: [Item]
    
    init(title: String, type: Type, items: [Item]) {
        self.title = title
        self.type = type
        self.items = items
    }
}
