//
//  Login.swift
//  ToolboxCarousel
//
//  Created by Daniel on 09/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation

struct Login {
    var sub: String
    var token: String
    var type: String
    
    init(sub: String, token: String, type: String) {
        self.sub = sub
        self.token = token
        self.type = type
    }
}
