//
//  Item.swift
//  ToolboxCarousel
//
//  Created by Daniel on 10/10/2019.
//  Copyright © 2019 Daniel. All rights reserved.
//

import Foundation

struct Item {
    var title: String
    var imageUrl: String
    var videoUrl: String
    var description: String
    
    init(title: String, imageUrl: String, videoUrl: String, description: String) {
        self.title = title
        self.imageUrl = imageUrl
        self.videoUrl = videoUrl
        self.description = description
    }
}
