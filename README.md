# Toolbox Test

Toolbox test app that shows a list of items in a carousel.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

## Built With

This app was build with the following technologies:

* [Swift](https://developer.apple.com/swift/) - Programming Language in it's 5th version.
* [Cocoapods](https://cocoapods.org) - Dependency Management.
* [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP networking library written in Swift.
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage) - Image component library for Alamofire.
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - Library to manage JSON data in Swift.

## Prerequisites

You need an **MAC OSX** system to execute successfully this project.

You need to have installed [XCode](https://developer.apple.com/xcode/) to execute the project.

In case you need to download the libraries used in this project, you need to have installed [Cocoapods](https://cocoapods.org).

## Installing and executing

### Installing

Download the project by clonning this repository into your machine:

```
git clone https://gitlab.com/apps-iOS/toolbox-carousel.git ToolboxCarousel
```

Access to the directory already created:

```
cd ToolboxCarousel
```

Open the project by clicking the file `ToolboxCarousel.xcworkspace`, identified with a **white icon**.

If you don't have the `ToolboxCarousel.xcworkspace` file, perhaps you need to install the libraries with **Cocoapods**:

```
pod install
```

Afther that, execute the `ToolboxCarousel.xcworkspace` file.

### Executing

Once inside the project, select an iPhone simulator to execute the project. Then, click on **Play button**.

*(The app was builded for iPhone only, so, is recommended to execute it on an iPhone simulator).*

For login the app, you must insert the following code: `ToolboxMobileTest`.

## Author

**Daniel Gomez Sevald** - danielgsevald@gmail.com